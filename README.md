## Zadanie testowe IFX

Krótka instrukcja i wyjaśnienia ;)

1. Aby odpalić projekt ściągamy repo i wystarcza **gulp watch** w konsoli

---

Tworzyłem prostego gulpa do obsługi css - bo jak zrozumiałem nie to miało być istotą zadania, stąd też brak minifikacji itp, a wszystkie taski gulpowe są w 1 pliku - nie ma też konfigów pod dev i prod itd.

Nie przykładałem też dużej wagi do pixel perfect ponieważ pliki graficzne to raczej makieta niż finalny produkt - jeśli źle to zrozumiałem proszę poprawcie mnie ;) - starałem się zrobić zadanie tak jak na makietach, ale nie patrzyłem na pojedyncze pixele.

Mam nadzieję, że sprawdzacie samego html i css tak jak myślałem otrzymując zadanie nazwane "przygotować stronę korzystając tylko z HTMLa i CSSa"

Nie rozbijałem też plików na templatki z tego samego powodu - nie ma tu dużo html i uznałem, że będzie łatwiej sprawdzać go w jednym pliku.

Z wyjaśnień to chyba tyle, także dzięki i czekaj na jakiegoś feedbacka.
