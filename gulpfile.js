/**
 * Paths to project folders
 */

var paths = {
	input: 'src/',
    output: 'dist/',
    copy: {
		input: 'src/copy/**/*',
		output: 'dist/'
	},
	styles: {
		input: 'src/styles/scss/main.scss',
		output: 'dist/css/'
    },
    reload: './dist/'
};

// General
var {src, dest, watch, series, parallel} = require('gulp');
var del = require('del');
var rename = require('gulp-rename');

// Styles
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');

// BrowserSync
var browserSync = require('browser-sync');

/**
 * Gulp Tasks
 */

// Copy static files into output folder
var copyFiles = function (done) {

	// Copy static files
	return src(paths.copy.input)
		.pipe(dest(paths.copy.output));

};

// Remove pre-existing content from output folders
var cleanDist = function (done) {

	// Clean the dist folder
	del.sync([
		paths.output
	]);

	// Signal completion
	return done();

};

// Process  Sass files
var buildStyles = function (done) {

	// Run tasks on all Sass files
	return src(paths.styles.input)
		.pipe(sass({
			outputStyle: 'expanded',
			sourceComments: true
		}))
		.pipe(prefix({
			browsers: ['last 2 version', '> 0.25%'],
			cascade: true,
			remove: true
        }))
        .pipe(rename('main.css'))
		.pipe(dest(paths.styles.output));
};

// Watch for changes to the src directory
var startServer = function (done) {

	// Initialize BrowserSync
	browserSync.init({
		server: {
			baseDir: paths.reload
		}
	});

	// Signal completion
	done();

};

// Reload the browser when files change
var reloadBrowser = function (done) {
	browserSync.reload();
	done();
};

// Watch for changes
var watchSource = function (done) {
	watch(paths.input, series(exports.default, reloadBrowser));
	done();
};
  
/**
 * Export Tasks
 */

// Default task
// gulp
exports.default = series(
	cleanDist,
	parallel(
        buildStyles,
        copyFiles
	)
);

// gulp watch
exports.watch = series(
    exports.default,
    startServer,
	watchSource
);
